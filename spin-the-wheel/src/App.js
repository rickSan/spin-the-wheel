import './index.css';
import './App.css';

import { PrizeWheel } from "./components/PrizeWheel";
import { Button } from "./components/Button";
import { Text } from "./components/Text.js";

function App() {
  return (
      <div className="container">
          <PrizeWheel />
          <Text />
          <Button />
      </div>
  );
}

export default App;

