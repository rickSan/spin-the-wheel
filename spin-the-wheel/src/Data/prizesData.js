export const prizesData = [
  {
    "prize": "10$",
    "bgColor": "#00376E",
    "textColor": "white"
  },
  {
    "prize": "ZERO",
    "bgColor": "#F84B25",
    "textColor": "black"
  },
  {
    "prize": "2$",
    "bgColor": "#F8B214",
    "textColor": "black"
  },
  {
    "prize": "50$",
    "bgColor": "#00B88F",
    "textColor": "black"
  },
  {
    "prize": "1$",
    "bgColor": "#00376E",
    "textColor": "white"
  },
  {
    "prize": "5$",
    "bgColor": "#F84B25",
    "textColor": "black"
  },
  {
    "prize": "20$",
    "bgColor": "#F8B214",
    "textColor": "black"
  },
  {
    "prize": "JACKPOT",
    "bgColor": "#00B88F",
    "textColor": "black"
  },
  {
    "prize": "15$",
    "bgColor": "#00376E",
    "textColor": "white"
  },
  {
    "prize": "100$",
    "bgColor": "#F84B25",
    "textColor": "black"
  },
  {
    "prize": "1$",
    "bgColor": "#F8B214",
    "textColor": "black"
  },
  {
    "prize": "5$",
    "bgColor": "#00B88F",
    "textColor": "black"
  },
];