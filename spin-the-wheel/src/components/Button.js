import React from "react";
import '../index.css';
import '../App.css';

import { startSpin } from '../scripts/script.js';

export const Button = () => {
    return (
        <button id="button" onClick={ startSpin }>
            Start
        </button>
    );
};