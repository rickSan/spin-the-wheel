import React from "react";
import '../index.css';
import "../App.css";
import { prizesData } from "../Data/prizesData";

export const PrizeWheel = (spinState) => {
    return (
        <div className="prizesWheelContainer">
            <div className="arrow"></div>
            <ul className="prizesWheel">
                {prizesData.map((data, key) => {
                    return (
                        <li key={key} style={{backgroundColor: data.bgColor, transform: "rotate(" + key*30 + "deg) skewY(-60deg)", fontSize: "1em", fontWeight: 800, color: data.textColor}}>
                            <div className="text">{data.prize}</div>
                        </li>
                    );
                })}
            </ul>
        </div>
    );
};