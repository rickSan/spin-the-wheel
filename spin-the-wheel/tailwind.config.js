module.exports = {
  content: [
      "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      screens: {
        xs: '383px',
        sm: '480px',
        md: '768px',
        lg: '991px',
        xl1: '1170px',
        xl: '1440px',
      },
      colors: {
        transparent: 'transparent',
        current: 'currentColor',
      }
    },
  },
  plugins: [],
}
